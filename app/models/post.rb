class Post < ApplicationRecord
  belongs_to :user
  has_many :post_category_relations
  has_many :categories, through: :post_category_relations
  has_many :comments, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 50 }
  validates :name, length: { maximum: 50 }
  validates :text, length: { maximum: 1000 }

  def self.search(search)
    if search
      Post.joins(:comments).includes(:comments).where(['comments.content LIKE ?', "%#{search}%"])
      .or(Post.joins(:comments).includes(:comments).where(['posts.content LIKE ?', "%#{search}%"]))
      .or(Post.joins(:comments).includes(:comments).where(['posts.text LIKE ?', "%#{search}%"]))
    else
      all
    end
  end
end