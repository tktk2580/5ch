class HomesController < ApplicationController
  def home
    @posts = Post.paginate(page: params[:page]).search(params[:search])
    @post = current_user.posts.build if signed_in?
  end
end
