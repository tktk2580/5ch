class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  
  def show
    @post = Post.find(params[:id])
    @user = @post.user
    @comment = Comment.new
    # @comments = @post.comments
  end
  
  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to root_url
    else
      redirect_to root_url
    end
  end
  
  def edit
    @post = Post.find(params[:id])
  end
  
  def update
    @post = Post.find(params[:id])
    if @post.update_attributes(post_params)
      redirect_to root_url
    else
      render 'edit'
    end
  end
  
  def destroy
    @post.destroy
    redirect_to root_url
  end
  
  private

    def post_params
      params.require(:post).permit(:content, :name, :email, :text, category_ids: [])
    end
    
    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
end