require 'rails_helper'

RSpec.feature 'CommentsNew', type: :feature do
  let!(:user) { FactoryBot.create(:tom) }
  let!(:comment_user) { FactoryBot.create(:micher) }
  let!(:post) { FactoryBot.create(:orange) }
  let!(:post2) { FactoryBot.create(:pink) }
  let!(:com1) { FactoryBot.create(:yam) }
  let!(:com2) { FactoryBot.create(:momo) }

  scenario 'search post title' do
    sign_in_as(user)
    visit root_path
    expect(page).to have_content '5chについて'
    expect(page).to have_content '2chについて'
    fill_in 'search-text', with: '5ch'
    click_button '検索'
    expect(page).to have_content '5chについて'
    expect(page).not_to have_content '2chについて'
  end
  
  scenario 'search post content' do
    sign_in_as(user)
    visit root_path
    expect(page).to have_content '5チャンネルは楽しい'
    expect(page).to have_content '2チャンネルはおもんない'
    fill_in 'search-text', with: '楽しい'
    click_button '検索'
    expect(page).to have_content '5チャンネルは楽しい'
    expect(page).not_to have_content '2チャンネルはおもんない'
  end
  
  scenario 'search post content' do
    sign_in_as(user)
    visit root_path
    expect(page).to have_content 'ボチボチです'
    expect(page).to have_content '普通です'
    fill_in 'search-text', with: 'ボチボチ'
    click_button '検索'
    expect(page).to have_content 'ボチボチです'
    expect(page).not_to have_content '普通です'
  end
end
