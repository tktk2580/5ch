require 'rails_helper'

RSpec.feature 'CommentsNew', type: :feature do
  let(:user) { FactoryBot.create(:tom) }
  let(:comment_user) { FactoryBot.create(:micher) }
  let(:post) { FactoryBot.create(:orange, user_id: comment_user.id) }

  scenario 'comment correctly and delete comment' do
    expect do
      sign_in_as(user)
      visit post_path(post.id)
      expect(page).to have_content '5chについて'
      fill_in 'comment[name]', with: "tom"
      fill_in 'comment[content]', with: "あまり使いません"
      click_button '書き込む'
      expect(current_path).to eq "/posts/1"
      expect(page).to have_content "tom"
      expect(page).to have_content "あまり使いません"
    end.to change(Comment, :count). by(1)
    click_link '削除'
    expect(current_path).to eq "/posts/1"
    expect(page).not_to have_content "tom"
    expect(page).not_to have_content "あまり使いません"
  end

  scenario 'res content should not be empty' do
    expect do
      sign_in_as(user)
      visit post_path(post.id)
      expect(page).to have_content '5chについて'
      fill_in 'comment[content]', with: ""
      click_button '書き込む'
      expect(current_path).to eq "/posts/1"
    end.to change(Comment, :count). by(0)
  end

  scenario 'res content is up to 1000' do
    expect do
      sign_in_as(user)
      visit post_path(post.id)
      expect(page).to have_content '5chについて'
      fill_in 'comment[content]', with: 'a' * 1001
      click_button '書き込む'
      expect(current_path).to eq "/posts/1"
    end.to change(Comment, :count). by(0)
  end
end
