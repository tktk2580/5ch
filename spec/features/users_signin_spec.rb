require 'rails_helper'

RSpec.feature 'UserSignup', type: :feature do
  let(:user) { FactoryBot.create(:tom) }

  scenario 'login is successfly to logout' do
    visit new_user_session_path
    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: user.password
    click_button 'Log in'
    expect(current_path).to eq root_path
    expect(page).not_to have_content "ログイン"
    click_link "ログアウト"
    expect(page).to have_content "ログイン"
    expect(current_path).to eq root_path
  end

  scenario 'address is not inputted' do
    visit new_user_session_path
    fill_in 'user[email]', with: ''
    fill_in 'user[password]', with: user.password
    click_button 'Log in'
    expect(current_path).to eq "/users/sign_in"
  end

  scenario 'address and password is wrong' do
    visit new_user_session_path
    fill_in 'user[email]', with: 'abcd@example.com'
    fill_in 'user[password]', with: 'abcdefg'
    click_button 'Log in'
    expect(current_path).to eq "/users/sign_in"
  end
end
