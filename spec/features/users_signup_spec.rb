require 'rails_helper'

RSpec.feature 'UserSignup', type: :feature do
  let!(:user) { FactoryBot.create(:tom) }

  scenario 'sign_up is successfly' do
    @user = User.new(email: "user@example.com", password: '123456')
    expect do
      visit new_user_registration_path
      fill_in 'user[email]', with: @user.email
      fill_in 'user[password]', with: @user.password
      fill_in 'user[password_confirmation]', with: @user.password
      click_button 'Sign up'
      expect(current_path).to eq root_path
    end.to change(User, :count). by(1)
  end

  scenario 'errors are displayed correctly' do
    expect do
      visit new_user_registration_path
      fill_in 'user[email]', with: ""
      fill_in 'user[password]', with: ""
      fill_in 'user[password_confirmation]', with: ""
      click_button 'Sign up'
      expect(current_path).to eq "/users"
      expect(page).to have_content "Email can't be blank"
      expect(page).to have_content "Password can't be blank"
    end.to change(User, :count). by(0)
  end

  scenario 'user has already been taken' do
    expect do
      visit new_user_registration_path
      fill_in 'user[email]', with: user.email
      fill_in 'user[password]', with: user.password
      fill_in 'user[password_confirmation]', with: user.password_confirmation
      click_button 'Sign up'
      expect(current_path).to eq "/users"
      expect(page).to have_content "Email has already been taken"
    end.to change(User, :count).by(0)
  end
end
