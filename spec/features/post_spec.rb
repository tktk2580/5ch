require 'rails_helper'

RSpec.feature 'MicropostsNew', type: :feature do
  let!(:user) { FactoryBot.create(:tom) }

  scenario 'making new thread is successfly and edit delete thread' do
    @post = Post.new(content: "5chについて",
                     name: "tommm" ,
                     text: "5チャンネルは楽しい")
    expect do
      expect do
        sign_in_as(user)
        visit root_path
        fill_in 'post[content]', with: @post.content
        fill_in 'post[name]', with: @post.name
        fill_in 'post[text]', with: @post.text
        click_button '投稿'
        expect(current_path).to eq root_path
      end.to change(Post, :count). by(1)
      expect(page).to have_content "5chについて"
      click_link "5chについて"
      expect(current_path).to eq "/posts/1"
      expect(page).to have_content "書き込み欄"
      click_link "Home"
      expect(current_path).to eq root_path
      click_link 'edit'
      expect(current_path).to eq "/posts/1/edit"
      fill_in 'post[content]', with: "5ch???"
      click_button '投稿'
      expect(current_path).to eq root_path
      expect(page).to have_content "5ch???"
      click_link 'delete'
      expect(current_path).to eq root_path
    end.to change(Post, :count). by(0)
  end

  scenario 'errors are displayed correctly' do
    @post = Post.new(content: "5chについて",
                     name: "tommm" ,
                     text: "5チャンネルは楽しい")
    expect do
      sign_in_as(user)
      visit root_path
      fill_in 'post[content]', with: ""
      fill_in 'post[name]', with: ""
      fill_in 'post[text]', with: ""
      click_button '投稿'
      expect(current_path).to eq root_path
    end.to change(Post, :count). by(0)
  end
end
