FactoryBot.define do
  factory :orange, :class => "Post" do
    content { '5chについて' }
    name { 'tommm' }
    text { '5チャンネルは楽しい' }
    user_id { '1' }
  end
  factory :pink, :class => "Post" do
    content { '2chについて' }
    name { 'mic' }
    text { '2チャンネルはおもんない' }
    user_id { '2' }
  end
end
