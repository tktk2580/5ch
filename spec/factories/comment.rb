FactoryBot.define do
  factory :yam, :class => "Comment" do
    content { 'ボチボチです' }
    name { 'yam' }
    user_id { '1' }
    post_id { '2' }
  end
  factory :momo, :class => "Comment" do
    content { '普通です' }
    name { 'momo' }
    user_id { '2' }
    post_id { '1' }
  end
end