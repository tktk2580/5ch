FactoryBot.define do
  factory :tom, class: :User do
    email { 'foobar@email.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
    id { '1' }
  end
  factory :micher, class: :User do
    email { ' michael@example.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
  end
end
