require 'rails_helper'

RSpec.describe 'Posts', type: :request do

  let(:user) { FactoryBot.build(:tom) }
  let(:post) { FactoryBot.build(:orange) }

  describe 'GET /microposts' do
    context 'ユーザーを消した時' do
      it 'マイクロポストも一緒に消される' do
        expect do
          user.posts.build!
          user.destroy
        end
        expect(Post.where(id: 1).count).to eq 0
      end
    end
  end
end
