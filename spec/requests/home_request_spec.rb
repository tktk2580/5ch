require 'rails_helper'

RSpec.describe "HomeController", type: :request do
  describe 'GET #index' do

    it "responds successfully" do
      get root_path
      expect(response).to be_success
      expect(response.status).to eq 200
      expect(response.body).to match("5channel")
    end
  end
end
