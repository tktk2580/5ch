User.create!(email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar")

5.times do |n|
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(email: email,
               password:              password,
               password_confirmation: password)
end

Category.create!(name: "インターネット")
Category.create!(name: "エンタメ")
Category.create!(name: "生活・文化")
Category.create!(name: "社会・経済")
Category.create!(name: "健康・医療")
Category.create!(name: "趣味")
Category.create!(name: "住まい")
Category.create!(name: "スポーツ")
Category.create!(name: "美容")
Category.create!(name: "ファッション")
Category.create!(name: "恋愛・結婚")
Category.create!(name: "その他")

name = "名なしさん"
text = "please"
users = User.order(:created_at).take(5)
1.times do
  content = Faker::Name.name
  users.each { |user| user.posts.create!(content: content, name: name, text: text) }
end

