class AddColumnToPosts < ActiveRecord::Migration[5.1]
  def change
    add_column :posts, :name, :string, null: false, default: "名無しさん"
    add_column :posts, :text, :string
  end
end
