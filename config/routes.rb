Rails.application.routes.draw do
  devise_for :users
  root 'homes#home'
  resources :posts,          only: [:show, :create, :edit, :update, :destroy] do
    resources :comments, only: [:create, :destroy]
  end
end